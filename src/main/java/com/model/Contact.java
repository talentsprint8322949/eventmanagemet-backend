package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//ContactForm.java
@Entity
public class Contact {
 public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getGuestCount() {
		return guestCount;
	}
	public void setGuestCount(String guestCount) {
		this.guestCount = guestCount;
	}
//	public String getDateOfEvent() {
//		return dateOfEvent;
//	}
//	public void setDateOfEvent(String dateOfEvent) {
//		this.dateOfEvent = dateOfEvent;
//	}
	public String getMoreDetails() {
		return moreDetails;
	}
	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}
	@Id@GeneratedValue
	private int contactId;
public int getContactId() {
		return contactId;
	}
	public void setContactId(int contactId) {
		this.contactId = contactId;
	}
private String name;
 private String email;
 private String contactNumber;
 private String location;
 private String guestCount;
// private String dateOfEvent;
 private String moreDetails;

 // Getters and setters
}

