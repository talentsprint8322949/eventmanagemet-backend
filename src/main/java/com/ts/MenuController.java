package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.MenuDAO;
//import com.model.Makeup;
import com.model.Menu;

@CrossOrigin
@RestController
public class MenuController {
	
	@Autowired
	
	MenuDAO menuDAO;
	
	@PostMapping("/registerMenu")
	public void registerMenu(@RequestBody Menu menu){
		menuDAO.registerMenu(menu);
	}
	
	@GetMapping("/getMenu")
	
	public List<Menu> getMenu(){
		return menuDAO.getMenu();
	}
	
	@GetMapping("/getMenuByName/{category}")
	public List<Menu> getMenuByName(@PathVariable String category){
		return menuDAO.getMenuByName(category);
	}
	
	@PutMapping("/updateItem")
	public Menu updatePackage(@RequestBody Menu menu){
		menuDAO.updateItem(menu);
		return menu;
}
	@DeleteMapping("/deleteItemById/{id}")
	public void deleteItemById(@PathVariable int id){
		menuDAO.deleteItemById(id);
	}

}
