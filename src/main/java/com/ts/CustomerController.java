package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CustomerDAO;
import com.model.Customer;

@CrossOrigin(origins="http://localhost:4200")
	@RestController
	public class CustomerController {

		@Autowired
		
		CustomerDAO customerDAO;
		@PostMapping("/getRegister")
		public void getRegister(@RequestBody Customer customer){
			customerDAO.getRegister(customer);
		}
		
		@RequestMapping("/getAllCustomers")
		
		public List<Customer>getAllEmployee(){
			return customerDAO.getAllCustomers();
		}
	
		@GetMapping("/emailIdPassword/{emailId}/{password}")
		public Customer getEmpByNameEmail(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
			Customer customer = customerDAO.getCustomerByNameEmail(emailId, password);
		
			if(customer!=null){
				return customer;
			}
			return new Customer();
		}
		
		
	
	}
