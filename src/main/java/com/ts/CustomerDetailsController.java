package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CustomerDetailsDAO;
import com.model.CustomerDetails;
//import com.model.Makeup;

@CrossOrigin(origins="http://localhost:4200")
@RestController
public class CustomerDetailsController {

	@Autowired
	
	CustomerDetailsDAO customerDetailsDAO;
	
	
	@PostMapping("/bookEvent/{customerId}")
	public String bookEvent(@RequestBody CustomerDetails customerdetails,@PathVariable int customerId){
		customerDetailsDAO.bookEvent(customerdetails, customerId);
		return "Stored Sucessfully";
	}
	@GetMapping("/getAllCustomerDetails")
	public List<CustomerDetails> getAllCustomerDetails(){
		return customerDetailsDAO.getAllCustomerDetails();
	}
	
	@GetMapping("/getCustomerDetailsById/{customerId}")
	
	public List<CustomerDetails> getById(@PathVariable int customerId){
		return customerDetailsDAO.getCustomerDetailsById(customerId);
	
	
}

}
//@RequestMapping("/getPackageById/{id}")
//public Makeup getById(@PathVariable int id){
//	Makeup makeup= makeupDAO.getPackageById(id);
//	if(makeup!=null){
//		return makeup;
//	}
//	return new Makeup();
//}


