package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CartDAO;
import com.model.Cart;
import com.model.CustomerDetails;

@CrossOrigin(origins="http://localhost:4200")
@RestController
public class CartController {

	@Autowired
	
	CartDAO cartDAO;
	
	@PostMapping("/regCartbyCustId/{customerId}")
	
	public String regCartbyCustId(@RequestBody Cart cart,@PathVariable int customerId){
	cartDAO.regCartbyCustId(cart, customerId);
	return "Stored Sucessfully";
	}
	
	@GetMapping("/getAllProducts")
	
	public List<Cart> getAllProducts(){
		return cartDAO.getAllProducts();
	}
	
	
@GetMapping("/getProductsById/{customerId}")
	
	public List<Cart> getProductsById(@PathVariable int customerId){
	return cartDAO.getProductsById(customerId);
}
	
	@DeleteMapping("/deleteByItemId/{cartId}")
	public void deleteByItemId(@PathVariable int cartId){
		cartDAO.deleteByItemId(cartId);
	}
	
	
	@DeleteMapping("/deleteCartBycustId/{customerId}")
	
	public String deleteCartBycustId(@PathVariable int customerId){
		cartDAO.deleteCartItemsBycustId(customerId);
		return "All cart items deleted sucessfully";
	}
}
