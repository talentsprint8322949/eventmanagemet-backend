package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ServicesDAO;
import com.model.Services;

@CrossOrigin(origins="http://localhost:4200")
@RestController
public class ServiceController {

	@Autowired
	
	ServicesDAO servicesDAO;
	
	@PostMapping("/registerServices")
	
	public void registerServices(@RequestBody Services service){
		servicesDAO.registerService(service);
	}
	
	@GetMapping("/getAllServices")
	
	public List<Services> getAllServices(){
		return servicesDAO.getAllServices();
	}
	
	@GetMapping("/getServicesByCategory/{category}")
	public List<Services> getServicesByCategory(@PathVariable String category){
		return servicesDAO.getServicesByCategory(category);
	}
	
	
	@GetMapping("/getServicesByName/{name}")
	public List<Services> getServicesByName(@PathVariable String name){
		return servicesDAO.getServiceByName(name);
	}
	
	@PutMapping("/updateService")
	public Services updateService(@RequestBody Services services){
		servicesDAO.updateServices(services);
		return services;
}
	@DeleteMapping("/deleteServicesById/{id}")
	public void deleteServicesById(@PathVariable int id){
		servicesDAO.deleteByServicesId(id);
	}
	
	
	
	
}
