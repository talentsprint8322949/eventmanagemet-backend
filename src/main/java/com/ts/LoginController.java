package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.LoginDAO;
import com.model.Login;

@CrossOrigin(origins="http://localhost:4200")
@RestController
public class LoginController {

	@Autowired 
	LoginDAO loginDAO;
	
	
	@PostMapping("/registerUser")
	public void registerUser(@RequestBody Login login){
		loginDAO.registerUser(login);
	}
	
	
	@GetMapping("/getAllUsers")
	
	public List<Login> getAllUsers(){
		return loginDAO.getAllUsers();
	}
}
