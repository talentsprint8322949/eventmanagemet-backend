package com.ts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.model.Contact;
//import com.model.ContactForm;


@CrossOrigin(origins="http://localhost:4200")
@RestController
@RequestMapping("/api/contact-form")
public class ContactController {
    @Autowired
    private JavaMailSender emailSender;

    @PostMapping
    @ResponseBody
    public String submitContactForm(@RequestBody Contact request) {
    	try {
            sendEmailToAdmin(request);
            return "Form submitted successfully";
        } catch (MailException e) {
           e.printStackTrace();
    	   return "Error submitting form: " + e.getMessage();
       }
    }

    private void sendEmailToAdmin(Contact request) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo("mokasandhya1@gmail.com"); 
        message.setSubject("New Contact Form Submission");
        message.setText("Name: " + request.getName() + "\n"+"Email: " + request.getEmail() + "\n"+"Contact Number: " + request.getContactNumber() + "\n"+"Location: " + request.getLocation() + "\n"+"Guest Count: " + request.getGuestCount() + "\n"+"More Details: " + request.getMoreDetails());
        emailSender.send(message);
    }
}