package com.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.FoodDAO;
import com.model.Food;

@CrossOrigin(origins="http://localhost:4200")
@RestController
public class FoodController {
	@Autowired
	
	FoodDAO foodDAO;
	@PostMapping("/registerItems")
	public void registerItems(@RequestBody Food food){
		foodDAO.registerItems(food);
	}
	
	@GetMapping("/getAllItems")
	
	public List<Food>getAllItems(){
		return foodDAO.getAllItems();
	}

}
