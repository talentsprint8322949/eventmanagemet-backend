package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Customer;
import com.model.CustomerDetails;
//import com.model.Makeup;


@Service
public class CustomerDetailsDAO {
	@Autowired  
	
	CustomerDetailsRepo customerDetailsRepo;
	public String  bookEvent(CustomerDetails customerdetails,int customerId){
		Customer customer = new Customer();
		customer.setCustomerId(customerId);
		customerdetails.setCustomer(customer);
		customerDetailsRepo.save(customerdetails);
		return "Book Services Registered Sucessfully";
	}
	
	public List<CustomerDetails> getAllCustomerDetails(){
		return customerDetailsRepo.findAll();
	}
	
//	public void getRegisterById(CustomerDetails customerdt){
//		customerDetailsRepo.save(customerdt);
//	}
//	
//	public List<CustomerDetails> getServicesByCustomer(){
//		return customerDetailsRepo.findAll();
//	}
	
	public List<CustomerDetails> getCustomerDetailsById(int customerId){
        return customerDetailsRepo.findByCustomer_CustomerId(customerId); 
    }

}
