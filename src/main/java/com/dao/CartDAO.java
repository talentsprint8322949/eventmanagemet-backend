package com.dao;

import java.util.List;

//import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.model.Cart;

@Service
@Transactional
public class CartDAO {

	
	@Autowired
	
	CartRepo cartRepo;
	
//	public void registerCart(Cart cart){
//		cartRepo.save(cart);
//	}
//	
	
	
//	public String regCartbyCustId(Cart cart,int customerId){
//		cart.getCustomer().setCustomerId(customerId);
//		cartRepo.save(cart);
//		return "Cart Items Added Sucessfully";
//	}
	
	public String regCartbyCustId(Cart cart,int customerId){
		cart.getCustomer().setCustomerId(customerId);
		cartRepo.save(cart);
		return "Items Addded Sucessfully";
		
    }
	
	public List<Cart> getAllProducts(){
		return cartRepo.findAll();
	}
	
	public List<Cart> getProductsById(int customerId){
		return cartRepo.findByCustomerCustomerId(customerId);
	}
	
	public String deleteByItemId(int cartId){
		cartRepo.deleteById(cartId);
		return "deleted";
	}
	
	public String deleteCartItemsBycustId(int customerId){
		cartRepo.deleteByCustomerCustomerId(customerId);
		return "Deleted SucessFully";
	}
	
}
