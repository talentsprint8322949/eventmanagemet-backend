package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Cart;

@Repository
public interface CartRepo extends JpaRepository<Cart,Integer> {
	 List<Cart> findByCustomerCustomerId(int customerId);
	 
	 void deleteByCustomerCustomerId(int customerId);
	
	
	

}
