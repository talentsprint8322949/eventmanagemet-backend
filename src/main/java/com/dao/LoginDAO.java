package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Login;

@Service
public class LoginDAO {
	
	@Autowired
	LoginRepo loginRepo;
	
	public void registerUser(Login login){
		loginRepo.save(login);
	}
 public List<Login> getAllUsers(){
	 return loginRepo.findAll();
 }
}
