package com.dao;

import java.util.HashMap;
import java.util.Map;

import com.model.Contact;

public class ContactDAO {

	
	 private Map<String, Contact> contactFormByEmail = new HashMap<>();

	    public Contact save(Contact contact) {
	        contactFormByEmail.put(contact.getEmail(), contact);
	        return contact;
	    }

	    public Contact ContactFormfindByEmail(String email) {
	        return contactFormByEmail.get(email);
	    }

	    public void deleteByEmail(String email) {
	        contactFormByEmail.remove(email);
	    }
}
