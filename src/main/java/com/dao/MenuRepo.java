package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Menu;

@Repository
public interface MenuRepo extends JpaRepository <Menu,Integer> {
	
	@Query("from Menu i where i.category = :category")
	List<Menu> getMenuByName(@Param ("category") String category);
	


}
