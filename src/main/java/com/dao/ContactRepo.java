package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.model.Contact;

public interface ContactRepo extends JpaRepository<Contact,Integer> {

}
