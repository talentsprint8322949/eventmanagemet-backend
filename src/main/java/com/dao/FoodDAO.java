package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Food;

@Service
public class FoodDAO {

	@Autowired
	FoodRepo foodRepo;
	
	public void registerItems(Food food){
		foodRepo.save(food);
	}
	
	public List <Food> getAllItems(){
		return foodRepo.findAll();
	}
}
