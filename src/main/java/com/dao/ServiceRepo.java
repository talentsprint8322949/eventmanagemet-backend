package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Services;

@Repository
public interface ServiceRepo extends JpaRepository<Services,Integer> {

	
	@Query("from Services s where s.category= :category")
	List<Services> getServicesByCategory(@Param ("category") String category);
	
	@Query("from Services s where s.name= :name")
	List<Services> getServiceByName(@Param ("name") String name);
}
