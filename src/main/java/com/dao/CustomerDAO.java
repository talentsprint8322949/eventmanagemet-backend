package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Customer;

@Service
public class CustomerDAO {

	@Autowired
	CustomerRepo customerRepo;
	
	public void getRegister(Customer customer){
		customerRepo.save(customer);
	}
	
	public List <Customer> getAllCustomers(){
		return customerRepo.findAll();
	}
	
	public Customer getCustomerByNameEmail(String emailId, String password) {
	    return customerRepo.getCustomerByNameEmail(emailId, password);
	}
	

}
