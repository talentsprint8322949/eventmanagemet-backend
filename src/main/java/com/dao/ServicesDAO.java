package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import com.model.Menu;
import com.model.Services;

@Service
public class ServicesDAO {

	@Autowired
	ServiceRepo serviceRepo;
	
	public void registerService(Services service){
		serviceRepo.save(service);
	}
	
	public List<Services> getAllServices(){
		return serviceRepo.findAll();
	}
	
	public List<Services> getServicesByCategory(String category){
		return serviceRepo.getServicesByCategory(category);
	}
	
	public List<Services> getServiceByName(String name){
		return serviceRepo.getServiceByName(name);
	}
	
	public Services updateServices(Services services){
		int id=services.getId();
		if(serviceRepo.findById(id).orElse(null)!=null){
			serviceRepo.save(services);
			return services;
		}
		return new Services();
	}
	public String deleteByServicesId(int id){
		serviceRepo.deleteById(id);
		return "deleted";
	}
	
}


