package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import com.model.Makeup;
import com.model.Menu;

@Service
public class MenuDAO {

	@Autowired
	MenuRepo menuRepo;
	
	public void registerMenu(Menu menu){
		menuRepo.save(menu);
	}
	
	public List<Menu> getMenu(){
		return menuRepo.findAll();
	}
	
	public List<Menu> getMenuByName(String category){
		return menuRepo.getMenuByName(category);
	}
	
	public Menu updateItem(Menu menu){
		int id=menu.getMenuId();
		if(menuRepo.findById(id).orElse(null)!=null){
			menuRepo.save(menu);
			return menu;
		}
		return new Menu();
	}
	public String deleteItemById(int id){
		menuRepo.deleteById(id);
		return "deleted";
	}
}
